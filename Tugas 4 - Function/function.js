//Nomor 1
console.log("Nomor 1 \n");
function teriak() {
  return "Halo Sanbers!"
}
 
console.log(teriak());
console.log("=================");

//Nomor 2
console.log("Nomor 2 \n");
function kalikan(num1, num2){
  return num1 * num2
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);
console.log("=================");

//Nomor 3
console.log("Nomor 3 \n");
function introduce(name, age, address, hobby){
  return "Nama saya " + name + ", umur saya " + age + " tahun" + " alamat saya di " + address + " dan saya punya hobi yaitu " + hobby + "!"
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);