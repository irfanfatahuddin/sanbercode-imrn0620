//Soal A
console.log("- Soal A -");
function bandingkan(number, bilangan){
    if (number < 0 || bilangan < 0 || number == bilangan){
      return -1;
    } else if(bilangan === undefined){
        return number;
    } else if(number === undefined && bilangan === undefined){
        return -1
    }
    else if (number > bilangan){
      return number;
    } else{
      return bilangan;
    }
  }
  console.log(bandingkan(10, 15));
  console.log(bandingkan(12, 12));
  console.log(bandingkan(-1, 10));
  console.log(bandingkan(112, 121));
  console.log(bandingkan(1));
  console.log(bandingkan());
  console.log(bandingkan("15", "18"));
  console.log("====================");

  //Soal B
  console.log("- Soal B -");
  function balikString(string){
    let word = '';
    for (let i = string.length - 1; i >= 0; i--){
      word += string[i];
    }
    return word;
  }
  console.log(balikString("abcde"));
  console.log(balikString("rusak"));
  console.log(balikString("racecar"));
  console.log(balikString("haji"));
  console.log("====================");