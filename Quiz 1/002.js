//Soal A
console.log("Soal A");
function descendingTen(num){
  let arr = [];
  if(num === undefined){
    return -1
  } else{
    for (let i = num; i > num - 10; i--){
      arr.push(i);
    }
  }
  return arr.sort(function(value1, value2){value1 > value2})
}
console.log(descendingTen(100));
console.log(descendingTen(10));
console.log(descendingTen());
console.log("====================");

//Soal B
console.log("Soal B");
function ascendingTen(num){
    let arr = [];
    if(num === undefined){
      return -1
    } else{
      for (let i = num; i < num + 10; i++){
        arr.push(i);
      }
    }
    return arr.sort(function(value1, value2){value1 > value2})
}
console.log(ascendingTen(11));
console.log(ascendingTen(21));
console.log(ascendingTen());
console.log("====================");

//Soal C
console.log("Soal C");
function conditionalAscDesc(reference, check){
    let arr = [];
    if(reference === undefined || check === undefined){
      return -1
    } else if(check%2 == 0){
      for (let i = reference; i > reference - 10; i--){
        arr.push(i);
      }
    } else{
      for (let i = reference; i < reference + 10; i++){
        arr.push(i);
      }
    }
    return arr.sort(function(value1, value2){value1 > value2})
}
console.log(conditionalAscDesc(20, 8));
console.log(conditionalAscDesc(81, 1));
console.log(conditionalAscDesc(31));
console.log(conditionalAscDesc());
  
