//Nomor 1
console.log("- Nomor 1 -");
function range(startNum, finishNum){
    var arr = [];
    if(startNum === undefined || finishNum === undefined){
      for(let i = 0; i > 0; i++){
        arr.push(i);
      }
      return -1
    } else if(startNum > finishNum){
      for(let i = finishNum; i < startNum + 1; i++){
        arr.push(i);
      }
      return arr.reverse();
    } else{
      for(let i = startNum; i < finishNum + 1; i++){
        arr.push(i)
      }
      return arr.sort();
    }
}
console.log(range(1, 9));
console.log(range(1))
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());
console.log("====================");

//Nomor 2
console.log("- Nomor 2 -");
function rangeWithStep(startNum, finishNum, step){
    var arr = [];
    if(startNum === undefined || finishNum === undefined){
      for(let i = 0; i > 0; i++){
        arr.push(i);
      }
      return -1
    } else if(startNum > finishNum){
      for(let i = startNum; i >= finishNum; i -= step){
        arr.push(i);
      }
      return arr.sort((startNum, finishNum, step)=>finishNum-startNum);
    } else{
      for(let i = startNum; i <= finishNum; i += step){
        arr.push(i)
      }
      return arr.sort();
    }
  }
  console.log(rangeWithStep(1, 10, 2));
  console.log(rangeWithStep(11, 23, 3));
  console.log(rangeWithStep(5, 2, 1));
  console.log(rangeWithStep(29, 2, 4));
  console.log("====================");

  //Nomor 3
  console.log("- Nomor 3 -");
  function sum(startNum, finishNum, step){
    var arr = [];
    
    if(startNum === undefined && finishNum === undefined && step === undefined){
      return 0
    } else if(finishNum === undefined && step === undefined){
      return startNum;
    } else if(step === undefined && startNum < finishNum){
      for(let i = startNum; i <= finishNum; i += 1){
        arr.push(i);
      }
      return arr.reduce((startNum, finishNum) => startNum + finishNum);
    } else if(step === undefined && startNum > finishNum){
      for(let i = startNum; i >= finishNum; i -= 1){
        arr.push(i);
      }
      return arr.reduce((startNum, finishNum) => startNum + finishNum);
    } else if(startNum > finishNum){
      for(let i = startNum; i >= finishNum; i -= step){
        arr.push(i);
      }
      return arr.reduce((startNum, finishNum) => startNum + finishNum);
    } else{
      for(let i = startNum; i <= finishNum; i += step){
        arr.push(i);
      }
      return arr.reduce((startNum, finishNum) => startNum + finishNum);
    }
  }
  console.log(sum(1,10));
  console.log(sum(5, 50, 2));
  console.log(sum(15,10));
  console.log(sum(20, 10, 2));
  console.log(sum(1));
  console.log(sum()); 
  
  
  
    
  
  
  