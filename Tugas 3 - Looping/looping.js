//nomor 1
console.log("Nomor 1")
var a = 2
var b = 0
var c = 22

console.log("LOOPING PERTAMA");
while(b < 20) { 
  b += a; 
  console.log(b + " - I love coding");
}
console.log("LOOPING KEDUA");
while(c > 2){
  c -= a;
  console.log(c + " - I will become a mobile developer");
}
console.log("==================================")

//nomor 2
console.log("Nomor 2")
for(var deret = 1; deret <= 20; deret ++) {
    if(deret%2 == 0){
      console.log(deret + " - Berkualitas");
    } else if (deret%3 == 0){
      console.log(deret + " - I love coding");
    } else{
      console.log(deret + " - Santai");
    }
  }
console.log("==================================")

//nomor 3
console.log("Nomor 3");
var a = "";
for(var baris = 0; baris < 8; baris++){
  for(var kolom = 0; kolom < 8; kolom++){
  a += '#';
  }
  a += '\n';
}
console.log(a);
console.log("==================================")

//Nomor 4
console.log("Nomor 4");
var a = "";
for(var baris = 0; baris < 8; baris ++){
  for(var kolom = 0; kolom < baris; kolom ++){
  a += '#';
  }
  a += '\n';
}
console.log(a);
console.log("==================================")

//Nomor 5
console.log("Nomor 5");
var a = "";
for(var baris = 0; baris < 8; baris ++){
  for(var kolom = 0; kolom < 8; kolom ++){
    if(baris%2 == 0){
        a += ' #';
    } else{
        a += '# ';
    }
  }
  a += '\n';
}
console.log(a);
